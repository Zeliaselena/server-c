#include <stdlib.h>
#include <stdio.h>
#include <time.h> 

char* lireFichier(char* fichier){
	char *file_contents;
	long input_file_size;
	
	FILE *input_file = fopen(fichier, "rb");
	
	fseek(input_file, 0, SEEK_END);
	
	input_file_size = ftell(input_file);
	rewind(input_file);
	
	file_contents = malloc(input_file_size * (sizeof(char)));
	
	fread(file_contents, sizeof(char), input_file_size, input_file);
	
	fclose(input_file);
	return file_contents;
}

int addlog(char* line){
	time_t rawtime;
	time(&rawtime);
	
	FILE *file;
	file = fopen("events.log","a+"); /* apend file (add text to
	a file or create a file if it does not exist.*/
	fprintf(file,"%s\t>%s\n",ctime(&rawtime),line); /*writes*/
	fclose(file); /*done!*/

	return 0;
}

