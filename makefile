CC=gcc
LD=gcc
CFLAGS=-Wall -W -Werror -g
LDFLAGS=
#Lenomdel’exécutableàfabriquer
EXE=tomdog
#LesvariablesHEADERS,CFILESetOBJSvontcontenirrespectivement
#lalistesdesfichiers.h,.cetlenomdesfichiers.oàfabriquer
#Onutiliseladirectiveparticulière $(wildcard...)quipermet
#deconstruireautomatiquementunelistedefichiers
HEADERS= $(wildcard *.h)
CFILES= $(wildcard *.c)
OBJS= $(CFILES:.c=.o)
.PHONY: all clean mrproper
all: $(EXE)
 $(EXE): $(OBJS)
	 $(LD)  $^  $(LDFLAGS) -o  $@
makefile.dep: $(CFILES) $(HEADERS)
	$(CC) -MM $(CFILES)> $@
#Cetterègleeffacelefichierdedépendancesetlesfichiers.o
clean:
	$(RM) $(OBJS)makefile.dep
#Cetterègleeffectuelaprécédenteeteffaceenplusl’exécutable
mrproper:clean
	$(RM) $(EXE)
#Onincluelefichierdedépendancequivacontenirlesrègles
#deconstructiondesfichiers.o
#S’iln’existepas,makeinvoqueautomatiquementlarègle
#quil’apourcible
include makefile.dep
