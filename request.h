#ifndef __REQUEST_H__
#define __REQUEST_H__
	char *substr(char *src,int pos,int len);
	char * fgets_or_exit ( char * buffer , int size , FILE * stream );
	int parse_http_request ( const char * request_line /*, http_request * request*/ );
	int parse_regex(const char * regex, const char * line);
#endif
