#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

#include "file.h"

/* Prépare un serveur sur le port "port" */
int creer_serveur(int port){
	int serveur;
	
	struct sockaddr_in saddr;
	
	//Creation de la structure
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(port);
	saddr.sin_addr.s_addr = INADDR_ANY;

	//Init du serveur
	serveur = socket(AF_INET, SOCK_STREAM, 0);
	if(serveur == -1){
		perror("Socket server");
		addlog("ERROR: socket server");
		return -1;
	}
		
	//activation de l'option SO_REUSEADDR'
	int optval = 1;
	if ( setsockopt ( serveur , SOL_SOCKET , SO_REUSEADDR , & optval , sizeof ( int )) == -1){
		perror ( " Can not set SO_REUSEADDR option " );
		addlog("ERROR: set REUSEADDR");		
	}
	
	//Bin du serveur
	if(bind(serveur, (struct sockaddr *)&saddr, sizeof(saddr)) == -1){
		perror("bind socket_server");
		addlog("ERROR: bind socket server");		
		return -1;
	}
	
	if(listen(serveur, 10) == -1){
		perror("listen socket server");
		addlog("ERROR: listen socket server");		
		return -1;
	}
		
	return serveur;
}

/* Envoi un message au client "client" */
void envoyer_message(int client, const char * message){
	write(client, message, strlen(message));
}

/* Traite un signal "SIGCHLD" */
void traitement_signal(int sig)
{
	if(sig > 0)
		printf("> Client disconnection (pid = %d)\n", waitpid(-1, NULL, 0));
}

/* Initialise les différents signaux */ 
void initialiser_signaux(void){
	struct sigaction sa;
	sa.sa_handler = traitement_signal;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	
	if (sigaction(SIGCHLD , &sa, NULL) == -1)
	{
		perror("sigaction(SIGCHLD)");
	}

	if(signal(SIGPIPE,SIG_IGN)==SIG_ERR){
	        perror("signal");
		addlog("ERROR: init signal");		
	}
}


