#ifndef __SOCKET_H__
#define __SOCKET_H__
	int  creer_serveur       (int port);
	void envoyer_message     (int client, const char * message);
	void traitement_signal   (int sig);
	void initialiser_signaux (void);
#endif
