#include <stdio.h>
#include<stdlib.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <regex.h>
#include <string.h>

#include "file.h"
#include "request.h"
#include "socket.h"

const int BUF_SIZE = 1024;

int main(int argc, char *argv[])
{	
	//const char * message_bienvenue = lireFichier("welcome");
	int port = 8000;
	
	if(argc > 1){
		port = atoi (argv[1]);
	}

	printf("1/3 - Starting TomDog server on port %d ...\n", port);
	addlog("Starting server.");
	printf("2/3 - Wait during the set up ...\n");
	addlog("Setting up the server.");
	
	int socket_client;
	pid_t pid;
	int serveur = creer_serveur(port);


        initialiser_signaux();
	printf("3/3 - Server ready !\n");
	
	printf("===========================\n");
	printf("-Server ready, waiting for-\n");
	printf("--------connections--------\n");
	printf("===========================\n");
	while(1){
		printf("\n");	
		socket_client = accept(serveur, NULL, NULL);
		
		pid=fork();
		if(pid == 0){
			char buf[BUF_SIZE];
			
			addlog("Client connected");
			printf(">New client : %d (pid = %d)\n", socket_client, getpid());		
			if(socket_client == -1){
				perror("accept");
				addlog("ERROR : accepting client.");
				return -1;
			}
			
			
			/*VERIFICATION REQUETE GET*/
			
			FILE *fClient = fdopen(socket_client, "w+");
			fgets_or_exit(buf, BUF_SIZE, fClient);
						
			if(parse_http_request ( buf /*, http_request * request*/ ) == 1){
				printf("Requete valide\n");
			} else {
				printf("Requete non valide.\n");
			}
			

			addlog("Client disconnected.");
			return 0;
		}
		else {
			close(socket_client);		
		}
	}
	return 0;
}
