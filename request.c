#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <sys/types.h>
#include <string.h>

char * fgets_or_exit ( char * buffer , int size , FILE * stream ){
	if(fgets(buffer, size, stream)){
		return buffer;
	}
	exit(-1);
	return NULL;
}

 
char *substr(char *src,int pos,int len) { 
  char *dest=NULL;                        
  if (len>0) {                            
    dest = (char *) malloc(len+1);        
    strncat(dest,src+pos,len);            
  }                                       
  return dest;                            
}

int parse_regex(const char * regex, const char * line){
	regex_t preg;
	if (regcomp (&preg, regex, REG_NOSUB | REG_EXTENDED) == 0)
	{
		if (regexec (&preg, line, 0, NULL, 0) == 0){
			return 0;
		}
	}
	return -1;
}


int parse_http_request ( char * request_line /*, http_request * request*/ ){
	const char * regex = "^GET [a-zA-Z0-9/._-~]+ HTTP/1.[01](\r\n|\n)$";
	if(parse_regex(regex, request_line) == -1)
		return 0; // Invalide
	
	int min =  (int) (request_line[strlen(request_line)-5] - '0');
	int max =  (int) (request_line[strlen(request_line)-3] - '0');
	
	char * result = strstr(request_line, "HTTP");
	int position = result - request_line;
	char * url = substr(request_line, 4, strlen(request_line) - position);
	
	printf("Version : %d,%d\n%s\n", max, min, url);

	return 1; // Valide
}


